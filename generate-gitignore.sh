#!/bin/bash

# Définition du type de projet avec une valeur par défaut 'node'
TYPE=${1:-node}

# Chemin vers le dossier des modèles
TEMPLATES_DIR="$HOME/scripts/gitignore-templates"

# Vérifier si le modèle existe
if [[ -f "$TEMPLATES_DIR/$TYPE.gitignore" ]]; then
    # Copier le modèle vers .gitignore
    cp "$TEMPLATES_DIR/$TYPE.gitignore" .gitignore
    echo ".gitignore pour '$TYPE' créé avec succès."
else
    echo "Aucun modèle trouvé pour '$TYPE'."
fi
